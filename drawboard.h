#ifndef _drawboard_h
#define _drawboard_h

#include <cstdlib>
#include "drawboard.h"
#include <iostream>
#include <vector>
#include <string>
#include <random>
using namespace std;

const int NUM_STANDARD_CUBES = 16;
const int NUM_BIG_BOGGLE_CUBES = 25;
const int NUM_DICE_SIDES = 6;
const int MAX_BOARD_DIMENSION = 5;

void drawBoard(char boardDimension[][MAX_BOARD_DIMENSION]);

void initializeBoard(char boardDimension[][MAX_BOARD_DIMENSION]);

char randomCharacter(int randomIndex, int index);

char randomizeCharacter(string letter);

string parseRandomizedString();

void shuffleBoxes(char boardDimension[][MAX_BOARD_DIMENSION]);

bool checkWord(char boardDimension[][MAX_BOARD_DIMENSION], int row, int col, int index, string word);

bool isValidWord(char boardDimension[][MAX_BOARD_DIMENSION], string word);

#endif