//File: Boggle
#include <iostream>
#include <vector>
#include <random>
#include <string>
#include <cstdlib>
#include "drawboard.h"
using namespace std;

void GiveInstructions()
{
    cout << endl << "The boggle board is a grid onto which I will randomly distribute " 
	 << "cubes. These 6-sided cubes have letters rather than numbers on the faces, " 
	 << "creating a grid of letters on which you try to form words. You go first, " 
	 << "entering all the words you can find that are formed by tracing adjoining " 
	 << "letters. Two letters adjoin if they are next to each other horizontally, " 
	 << "vertically, or diagonally. A letter can only be used once in the word. Words "
	 << "must be at least 4 letters long and can only be counted once. You score points "
	 << "based on word length: a 4-letter word is worth 1 point, 5-letters earn 2 "
	 << "points, and so on. After your puny brain is exhausted, I, the super computer, "
	 << "will find all the remaining words and double or triple your paltry score." << endl;
	
    cout << "\nHit return when you're ready...\n";
    // GetLine();
}

static void Welcome()
{
    cout << "Welcome!  You're about to play an intense game of mind-numbing Boggle. " 
	 << "The good news is that you might improve your vocabulary a bit.  The "
	 << "bad news is that you're probably going to lose miserably to this little "
	 << "dictionary-toting hunk of silicon.  If only YOU had a gig of RAM..." << endl << endl;
}

int main() 
{
	static char boardDimension[MAX_BOARD_DIMENSION][MAX_BOARD_DIMENSION];
	string userWord;
	Welcome();
	GiveInstructions();
	initializeBoard(boardDimension);
	shuffleBoxes(boardDimension);
	drawBoard(boardDimension);

	cout << "Please enter a valid boggle word: " << endl;
	getline(cin, userWord);
	cout << "---\n";
	cout << "You entered: " << userWord << endl;
	cout << "---\n";

	if(isValidWord(boardDimension, userWord)) {
		cout << "You entered a valid word." << endl;
	} else {
		cout << "You entered an invalid word." << endl;
	}

	drawBoard(boardDimension);
	return 0;
}