#include "drawboard.h"
#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <random>
#include <ctime>
using namespace std;

string StandardCubes[NUM_STANDARD_CUBES]  = {"AAEEGN", "ABBJOO", "ACHOPS", "AFFKPS", "AOOTTW", "CIMOTU", "DEILRX", "DELRVY",
 "DISTTY", "EEGHNW", "EEINSU", "EHRTVW", "EIOSST", "ELRTTY", "HIMNQU", "HLNNRZ"};

string BigBoggleCubes[NUM_BIG_BOGGLE_CUBES] = {"AAAFRS", "AAEEEE", "AAFIRS", "ADENNN", "AEEEEM", "AEEGMU", "AEGMNN", "AFIRSY", 
"BJKQXZ", "CCNSTW", "CEIILT", "CEILPT", "CEIPST", "DDLNOR", "DDHNOT", "DHHLOR", 
"DHLNOR", "EIIITT", "EMOTTT", "ENSSSU", "FIPRSY", "GORRVW", "HIPRRY", "NOOTUW", "OOOTTU"};

bool isValidWord(char boardDimension[][MAX_BOARD_DIMENSION], string word) {
	int row = 0;
	int col = 0;
	int index = 0;
	return checkWord(boardDimension, row, col, index, word);
}

bool checkWord(char boardDimension[][MAX_BOARD_DIMENSION], int row, int col, int index, string word){
	if(row < 0 || col < 0 || row >= MAX_BOARD_DIMENSION || col >= MAX_BOARD_DIMENSION) {
		return false;
	}
	printf("Row: %d | Col: %d | Index: %d | Word: %s\n", row, col, index, word.c_str());
	drawBoard(boardDimension);

	char initialCharacter;
	if(index == word.length()) {
		return true;
	}

	if(boardDimension[row][col] == '0') {
		return false;
	} 

	if(word.at(index) != boardDimension[row][col]) {
		return false;
	}

	if(word.at(index) == boardDimension[row][col]) {
		initialCharacter = boardDimension[row][col];
		boardDimension[row][col] = '0';
	}
	
	if(checkWord(boardDimension, row - 1, col - 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row - 1, col, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row - 1, col + 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row, col + 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row + 1, col + 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row + 1, col, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row + 1, col - 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	if(checkWord(boardDimension, row, col - 1, index + 1, word)) {
		boardDimension[row][col] = initialCharacter;
		return true;
	}

	boardDimension[row][col] = initialCharacter;
	return false;
}

void shuffleBoxes(char boardDimension[][MAX_BOARD_DIMENSION]) {
	random_device rd;
	mt19937 urd(rd());
	shuffle(&boardDimension[0][0], &boardDimension[0][0] + MAX_BOARD_DIMENSION * MAX_BOARD_DIMENSION, urd);
}

char randomizeCharacter(string letter) {
	int randCharacterInt = rand() % NUM_DICE_SIDES;
	return letter.at(randCharacterInt);
}

string parseRandomizedString() {
	string randomCharString = "";
	for (int i = 0; i < NUM_BIG_BOGGLE_CUBES; i++) {
		randomCharString = randomCharString + randomizeCharacter(BigBoggleCubes[i]);
	}
	return randomCharString;
}

void drawBoard(char boardDimension[][MAX_BOARD_DIMENSION]) {
	for (int i = 0; i < MAX_BOARD_DIMENSION; i++) {
		for (int j = 0; j < MAX_BOARD_DIMENSION; j++) {
			cout << boardDimension[i][j] << " ";
		}

		cout << "\n";
	}
	cout << "---\n";
}

void initializeBoard(char boardDimension[][MAX_BOARD_DIMENSION]) {
	string randomCharacterString = parseRandomizedString();
	int indexOfArray = 0;
	
	for (int i = 0; i < MAX_BOARD_DIMENSION; i++) {
		for (int j = 0; j < MAX_BOARD_DIMENSION; j++) {
			boardDimension[i][j] = randomCharacterString.at(indexOfArray);
			indexOfArray++;
		}
	}
}

// void swapIndex(char boardDimension[][MAX_BOARD_DIMENSION], int swapRow, int swapCol, int rowIndex, int colIndex) {
// 	char temp = boardDimension[rowIndex][colIndex];
// 	boardDimension[rowIndex][colIndex] = boardDimension[swapRow][swapCol];
// 	boardDimension[swapRow][swapCol] = temp;
// }

// void shuffleBoxes(char boardDimension[][MAX_BOARD_DIMENSION]) {
// 	int n = boardDimension.size(0);
// 	int m = boardDimension.size(1);

// 	int rand = rand();


// 	for (int row = 0; row < MAX_BOARD_DIMENSION; row++) {
// 		for (int col = 0; col < MAX_BOARD_DIMENSION; col++) {
// 			swapIndex(boardDimension, row + rand.Next(n - row), col + rand.Next(n - col), row, col);
// 		}
// 	}

// 	drawBoard(boardDimension);
// }

// void initializeBoard() {
// 	char boardDimension[MAX_BOARD_DIMENSION][MAX_BOARD_DIMENSION];
// 	string randomCharacterString = parseRandomizedString();
// 	int indexOfArray = 0;

// 	for (int i = 0; i < MAX_BOARD_DIMENSION; i++) {
// 		for (int j = 0; j < MAX_BOARD_DIMENSION; j++) {
// 			boardDimension[i][j] = randomCharacterString.at(indexOfArray);
// 			indexOfArray++;
// 		}
// 	}
// 	drawBoard(boardDimension);
// }
